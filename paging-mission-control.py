# Morgan Hewitt
# paging-mission-control
# https://gitlab.com/enlighten-challenge/paging-mission-control
# code runs from command line with: python3 paging-mission-control.py -f <file_name>.txt
# 20211210

import argparse, json
from datetime import datetime, timedelta

def ingest_data(file):
    '''
    Take the file input and create a dictionary of lists of dictionaries.
    '''
    nested_dict_keys = ['timestamp', 'satellite-id', 'red-high-limit', 'yellow-high-limit',
    'yellow-low-limit', 'red-low-limit', 'raw-value', 'component']
    data_dict = {}

    with open(file) as f:
        for line in f:
            # split each line of data using | as the delimeter
            data_list = list(line.strip().split('|'))

            # create the nested dictionary with their respective keys
            inner_dict = {}
            for idx in range(len(nested_dict_keys)):
                inner_dict[nested_dict_keys[idx]] = data_list[idx]

            # create the outer dictionary with the satellite-id as the key
            # append the nested dictionary to a list and add the list to the outer dictionary
            if int(data_list[1]) not in data_dict:
                data_dict[int(data_list[1])] = []
                data_dict[int(data_list[1])].append(inner_dict)
            else:
                data_dict[int(data_list[1])].append(inner_dict)
    return data_dict

def component_check(data_dict, key):
    '''
    Check the component limit values to determine if it is in 'danger' status
    i.e. RED HIGH or RED LOW.
    '''
    tmp_dict = {}
    if float(data_dict['raw-value']) < float(data_dict['red-low-limit']):
        tmp_dict.update(data_dict)
        tmp_dict['severity'] = 'RED LOW'
        keys.append(key)
        component_readings.append(tmp_dict)
         
    if float(data_dict['raw-value']) > float(data_dict['red-high-limit']):
        tmp_dict.update(data_dict)
        tmp_dict['severity'] = 'RED HIGH'
        keys.append(key)
        component_readings.append(tmp_dict)
    return keys

def satellite_check(key_list, key, data_dict, dict_key, match_val):
    '''
    Check to see if the same 'key' (satellite-id) has 3 or more hits
    from the component check.
    '''
    if key_list.count(key) >= 3:
        sat_match = [val for index, val in enumerate(data_dict) if int(data_dict[index][dict_key]) == match_val]
        return sat_match

def time_check(matches):
    '''
    Check to see if a satellite-id with 3 or more hits for a component malfunction
    has occurred within a 5 minute interval.
    '''
    time_format = '%Y%m%d %H:%M:%S.%f'
    start_time = datetime.strptime(matches[0]['timestamp'], time_format)
    if start_time + timedelta(minutes=5) >= datetime.strptime(matches[-1]['timestamp'], time_format):
        alert_dict = {'satelliteId': int(matches[0]['satellite-id']),
                    'severity': matches[0]['severity'], 'component': matches[0]['component'],
                    'timestamp': matches[0]['timestamp']}
        return alert_dict

def alert_system(component_dict):
    '''
    Add the alert to a list if there are 3 or more component malfunctions that
    occurred within a 5 minute interval.
    '''
    if len(component_dict) >=3:
        component_alert = time_check(component_dict)
        alert.append(component_alert)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--file", "-f", type=str, required=True)
    args = parser.parse_args()

    sat_data = ingest_data(args.file)

    keys = []
    component_readings = []
    alert = []
    
    for key, list_of_dicts in sat_data.items():
        for sat_dict in list_of_dicts:
            key_list = component_check(sat_dict, key)
            sat_matches = satellite_check(key_list, key, component_readings, 'satellite-id', key)
    if sat_matches:
        batt = [row for row in sat_matches if row['component'] == 'BATT']
        tstat = [row for row in sat_matches if row['component'] == 'TSTAT']
        alert_system(batt)
        alert_system(tstat)
    if alert:
        print(json.dumps(alert))